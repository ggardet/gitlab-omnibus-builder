FROM debian:stretch-slim as builder

# Install required packages
RUN apt-get update -q
RUN DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      zlib1g-dev \
      git \
      gcc \
      g++ \
      libssl1.0-dev \
      libreadline-dev \
      openssh-client \
      make \
      curl \
      ca-certificates \
      locales \
      libcurl4-openssl-dev \
      libexpat1-dev \
      gettext \
      unzip && apt-get -yq clean

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV GIT_VERSION 2.22.0
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-${GIT_VERSION} \
  && ./configure \
  && make all \
  && make install

ENV RUBY_VERSION 2.6.5
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.6/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-${RUBY_VERSION} \
  && ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi\
  && make \
  && make install

ENV RUBYGEMS_VERSION 2.6.13
RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-document

ENV BUNDLER_VERSION 1.17.3
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-document

RUN mkdir -p /opt/gitlab /var/cache/omnibus ~/.ssh

RUN git config --global user.email "packages@gitlab.com"
RUN git config --global user.name "GitLab Inc."

RUN curl -Lo /tmp/packer.zip https://releases.hashicorp.com/packer/1.2.5/packer_1.2.5_linux_amd64.zip && \
    unzip /tmp/packer.zip -d /usr/local/bin

RUN rm -rf /tmp/*

FROM debian:stretch-slim
MAINTAINER GitLab Inc. <support@gitlab.com>
COPY --from=builder / /
