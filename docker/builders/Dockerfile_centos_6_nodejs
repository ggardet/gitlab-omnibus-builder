FROM centos:centos6

# Install required packages
RUN yum update -y -q
RUN yum groupinstall -y Development Tools
RUN yum install -y epel-release centos-release-scl
RUN yum install -y -q \
      autoconf268 \
      autoconf \
      automake \
      autopoint \
      gcc \
      gcc-c++ \
      curl-devel \
      expat-devel \
      gettext-devel \
      openssl-devel \
      perl-devel \
      zlib-devel \
      make \
      libyaml-devel \
      libffi-devel \
      readline-devel \
      gdbm-devel \
      ncurses-devel \
      bzip2 \
      which \
      byacc \
      cmake \
      ccache \
      distcc \
      unzip \
      bzip2-devel \
      sqlite-devel \
      db4-devel \
      libpcap-devel \
      devtoolset-6

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
RUN localedef -c -f UTF-8 -i en_US en_US.UTF-8

# Building Python 2.7 
ENV PYTHON_VERSION 2.7.14
RUN curl -fsSL "http://python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz" \
	| tar -xJC /tmp \
        && cd /tmp/Python-${PYTHON_VERSION} \
        && ./configure --prefix=/usr/local --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib" \
        && make \
        && make altinstall \
        && cd /tmp \
        && rm -rf /tmp/Python-${PYTHON_VERSION}

ENV NODE_VERSION 12.4.0
RUN source scl_source enable devtoolset-6 && curl -fsSL "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/node-v${NODE_VERSION} \
  && ./configure --prefix=/usr/local/node \
  && make -j "$(nproc)" >/tmp/node.make.log || tail -1000 /tmp/node.make.log \
  && make -j "$(nproc)" install >/tmp/node.install.log || tail -1000 /tmp/node.install.log \
  && rm -rf /tmp/node-v${NODE_VERSION} && rm /tmp/node.make.log && rm /tmp/node.install.log \
  && /usr/local/node/bin/node --version
