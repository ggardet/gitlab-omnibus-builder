# Install Ruby for use by omnibus
#
# Ruby will be built from source and installed in /usr/local/bin.

case node['platform_family']
when 'debian'
  packages = %w{
    gcc libssl-dev libyaml-dev libffi-dev libreadline-dev zlib1g-dev
    libgdbm-dev libncurses5-dev make bzip2
  }
when 'rhel'
  packages = %w{
    gcc openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel
    gdbm-devel ncurses-devel make bzip2
  }
end

packages.each do |pkg|
  package pkg
end

ruby_build_install_dir = '/var/cache/ruby-build'

git ruby_build_install_dir do
  repository 'https://github.com/sstephenson/ruby-build.git'
  notifies :run, 'execute[install ruby-build]', :immediately
end

execute 'install ruby-build' do
  command './install.sh'
  cwd ruby_build_install_dir
  action :nothing
end

ruby_version = node['gitlab-omnibus-builder']['ruby_version']
execute "/usr/local/bin/ruby-build #{ruby_version} /usr/local" do
  not_if "/usr/local/bin/ruby --version | grep 'ruby #{ruby_version}'"
end

rubygems_version = node['gitlab-omnibus-builder']['rubygems_version']
execute "/usr/local/bin/gem update --system #{rubygems_version} --no-document" do
  not_if "/usr/local/bin/gem --version | grep '^#{rubygems_version}$'"
end

bundler_version = node['gitlab-omnibus-builder']['bundler_version']
execute "/usr/local/bin/gem install bundler --version #{bundler_version} --no-document" do
  not_if "/usr/local/bin/bundle --version | grep ' #{bundler_version}$'"
end
