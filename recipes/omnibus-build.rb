# Set up this instance for GitLab omnibus builds

# Build dependencies
#
case node['platform_family']
when 'debian'
  package 'build-essential'
  package 'zlib1g-dev'
when 'rhel'
  execute 'yum groupinstall -y Development Tools' do
    not_if 'yum grouplist  | sed "/^Available Groups/q" | grep -i "Development tools"'
  end
  package 'fakeroot'
  package 'zlib-devel'
  package 'which'
end

package 'byacc'
package 'cmake'

# Build user and directories
#
username = node['gitlab-omnibus-builder']['username']
builder_home = node['gitlab-omnibus-builder']['home']

user username do
  manage_home true
  home builder_home
  system true
  shell '/bin/bash'
  comment 'Omnibus Builder'
end

case node['platform_family']
when 'rhel'
  template File.join(builder_home, '.bashrc') do
    source 'bashrc.erb'
  end
end

# Set up more than one omnibus project
#
node['gitlab-omnibus-builder']['projects'].each do |project|
  directory File.join('/opt', project) do
    owner username
    group username
    recursive true
  end
end

directory '/var/cache/omnibus' do
  owner username
  group username
  recursive true
end

# The builder's home directory may contain secrets, let's restrict access to
# it.
directory builder_home do
  owner username
  group username
  mode '0700'
  recursive true
end

# SSH access to GitLab EE source code
#
directory File.join(builder_home, '.ssh') do
  owner username
  group username
  mode '0700'
  recursive true
end

# We just overwrite the known_hosts file; it only needs to 'know' how to
# connect to dev.gitlab.org.
file File.join(builder_home, '.ssh/known_hosts') do
  content "#{node['gitlab-omnibus-builder']['ee_source_host_key']}\n"
  owner username
  group username
end

# Uploading packages to AWS
#
if node['platform_family'] == 'rhel' && node['platform_version'] =~ /^7\./
  execute 'easy_install pip' do
    creates '/usr/bin/pip'
  end
else
  package 'python-pip'
end

execute 'pip install awscli' do
  not_if 'which aws'
end
